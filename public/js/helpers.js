getProfileName = function(client, nameStyle) {
  // this line is still duplicated in getProfilePic
  // TODO: fix later its late
  var idle = client.idle ? 'idle' : '';
  var profileName = $('<span>').addClass('profilename')
                               .addClass(idle)
                               .addClass(nameStyle)
                               .html(client.name);
  return profileName;
}

getProfilePic = function(client) {
  var picture = client.picture ? client.picture : DEFAULT_PICTURE;
  var idle = client.idle ? 'idle' : '';
  var profilePic = $('<img>').addClass('profilepic')
                             .addClass(idle)
                             .addClass('middle')
                             .attr('title', client.name)
                             .attr('src', picture);
  return profilePic;
}

getMockLocation = function() {
  return MOCK_LOCATIONS[Math.floor(Math.random() * MOCK_LOCATIONS.length)];
}

getProfileLocation = function(client) {
  return $('<span>').addClass('location')
                    .html(client.location ? client.location : getMockLocation());
}

idleSince = function(client) {
  return new Date().getTime() - client.idleFor;
}

getProfileIdle = function(client) {
  var idleSpan = $('<span>').addClass('idleSpan')
                            .attr('idleSince', idleSince(client));
  return client.idle ? idleSpan : '';
}

addClient = function(client, buddylist, nameStyle) {
  var profilePic = getProfilePic(client);
  var profileName = getProfileName(client, nameStyle);
  var profileLocation = getProfileLocation(client);
  var idleSince = getProfileIdle(client);

  var li = $('<li>').append(profilePic)
                    .append(profileName)
                    .append(idleSince)
                    .append(profileLocation);
  
  var inputField = $('textarea#inputfield');

  li.on('click', function() {
    inputField.val(inputField.val() + '@' + client.name + ' ')
              .setCursorPosition(inputField.val().length).focus();
  });

  buddylist.append(li);
}

isMemeCmd = function(cmd) {
  return (result['cmd'] == '/meme' &&
          result['topText'] != undefined &&
          result['bottomText'] != undefined &&
          ALLOWED_MEMES.hasOwnProperty(result['meme']) &&
          result['topText'].length <= MAX_MEME_TEXT_LENGTH &&
          result['bottomText'].length <= MAX_MEME_TEXT_LENGTH)
}

parseMemeCmd = function(stringCmd) {
  words = stringCmd.split(' ');
  text = stringCmd.split('"');
  result = {
    'topText': text[1],
    'bottomText': text[3],
    'cmd': words[0],
    'meme': words[1]
  }
  return result;
}

// used to update the meme canvas
// iterates through all the unprocessed memes and if they are not processed,
// memeify them
memeify = function() {
  canvasMemes = $('.meme');
  for (var i = 0; i < canvasMemes.length; i++) {
    canvas = $(canvasMemes[i]);
    if (canvas.attr('processed') != 'true') {
      // I have no idea why I have to specify canvas[0], but hey - it works
      var ctx = canvas[0].getContext("2d");
      var meme = canvas.attr('meme');
      var topText = canvas.attr('topText');
      var bottomText = canvas.attr('bottomText');
      var img = new Image();

      img.onload = function() {
        ctx.drawImage(img, 0, 0);
        ctx.fillStyle = "rgb(255, 255, 255)";
        ctx.font = "bold 20px sans-serif";
        ctx.fillText(topText, 5, 20);
        ctx.fillText(bottomText, 5, 145);
      }
      img.src = ALLOWED_MEMES[meme];
      canvas.attr('processed', true);
    }
  }
}

// https://en.wikipedia.org/wiki/Blink_element
blinkText = function() {
    for(i=0;i<document.all.tags('blink').length;i++) {
        s=document.all.tags('blink')[i];
        s.style.visibility=(s.style.visibility=='visible') ?'hidden':'visible';
    }
}
